package main

import (
	"fmt"
	"strconv"

	"github.com/andlabs/ui"

	_ "github.com/andlabs/ui/winmanifest"
)

func count(days int, country int, period int, dop1 bool, dop2 bool) float64 {
	var price1 float64 = 0
	if country == 0 && period == 0 {
		price1 = 100
	} else if country == 0 && period == 1 {
		price1 = 150
	} else if country == 1 && period == 0 {
		price1 = 160
	} else if country == 1 && period == 1 {
		price1 = 200
	} else if country == 2 && period == 0 {
		price1 = 120
	} else if country == 2 && period == 1 {
		price1 = 180
	}

	res := price1 * float64(days)

	if dop1 {
		res *= 1.2
	}

	if dop2 {
		res += float64(50 * days)
	}

	return res
}

func initGUI() {
	window := ui.NewWindow("Hello", 1000, 400, false)
	mainRow := ui.NewHorizontalBox()
	col1 := ui.NewVerticalBox()
	col1.Append(ui.NewLabel("Тури"), false)

	days := ui.NewEntry()

	row := ui.NewHorizontalBox()
	row.Append(ui.NewLabel("Кількість діб"), false)
	row.Append(days, false)
	col1.Append(row, false)

	row = ui.NewHorizontalBox()
	combobox1 := ui.NewCombobox()
	combobox1.Append("Болгарія")
	combobox1.Append("Німеччина")
	combobox1.Append("Польща")
	combobox1.SetSelected(0)
	row.Append(ui.NewLabel("Країна"), false)
	row.Append(combobox1, false)
	col1.Append(row, false)

	row = ui.NewHorizontalBox()
	combobox2 := ui.NewCombobox()
	combobox2.Append("Літо")
	combobox2.Append("Зима")
	combobox2.SetSelected(0)
	row.Append(ui.NewLabel("Пора року"), false)
	row.Append(combobox2, false)
	col1.Append(row, false)

	mainRow.Append(col1, false)

	col2 := ui.NewVerticalBox()
	col2.Append(ui.NewLabel("Доп послуги"), false)

	row = ui.NewHorizontalBox()
	checkbox1 := ui.NewCheckbox("Люкс")
	row.Append(checkbox1, false)
	col2.Append(row, false)

	row = ui.NewHorizontalBox()
	checkbox2 := ui.NewCheckbox("Персональний гід на кожен день")
	row.Append(checkbox2, false)
	col2.Append(row, false)

	mainRow.Append(col2, false)

	row = ui.NewHorizontalBox()
	button := ui.NewButton("Розрахувати")
	price := ui.NewLabel("")
	row.Append(price, false)
	row.Append(button, false)
	button.OnClicked(func(*ui.Button) {
		hdays, err := strconv.Atoi(days.Text())

		res := count(hdays, combobox1.Selected(), combobox2.Selected(), checkbox1.Checked(), checkbox2.Checked())
		price.SetText("Price: " + fmt.Sprintf("%f", res) + " $")

		if err != nil {
			// ... handle error
			panic(err)
		}
	})

	body := ui.NewVerticalBox()
	body.Append(mainRow, false)
	body.Append(row, false)

	window.SetChild(body)
	window.OnClosing(func(*ui.Window) bool {
		ui.Quit()
		return true
	})
	window.Show()
}
func main() {
	err := ui.Main(initGUI)
	if err != nil {
		panic(err)
	}
}
