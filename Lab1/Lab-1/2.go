package main

func main() {
	var (
		defaultInt32   int32                                // Цілочисельна знакова змінна
		userInt        uint32    = 10                       // Цілочисельна беззнакова змінна
		defaultfloat32 float32                              // Дійсна змінна
		third          bool      = true                     //Булева змінна
		first          complex64 = 0.37 + 1.72i             //Комплексна змінна
		first          string    = "First: line 1\nline 2"  // Строкова змінна
		second         string    = `Second: line 1\nline 2` // Строкова змінна
		letter         rune      = 'A'                      // Символьна змінна
		array          [5]int    = [5]int{1, 7, 4, 6, 3}    // Масив типу [5]int
	)
}
