package functions

import "testing"

func TestMin(t *testing.T) {
	x := Min(10, 20, 30)
	var res float64 = 10
	if x != res {
		t.Errorf("Тест не пройдений! Результат %f, (а повинен бути %f)", x, res)
	}
}

func TestAverage(t *testing.T) {
	x := Average(1, 2, -3)
	var res float64 = 0
	if x != res {
		t.Errorf("Тест не пройдений! Результат %f, (а повинен бути %f)", x, res)
	}
}

func TestEquation(t *testing.T) {
	x := Equation(10, 40)
	var res float64 = 4
	if x != res {
		t.Errorf("Тест не пройдений! Результат %f, (а повинен бути %f)", x, res)
	}
}
