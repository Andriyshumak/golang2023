package main

import (
	"fmt"
	"math"
	"time"
)

var random_min int64 = 0
var random_max int64 = 300
var random_count int64 = 10000
var a int64 = 69069
var c int64 = 1
var m int64 = (int64)(2 << 31)

var count_points float64 = 3

func rnd(x int64) (float64, int64) {
	var full_res int64 = (a*x + c) % m
	var period int64 = int64(math.Pow(10, count_points))
	var res float64 = float64(full_res%((random_max-random_min)*period))/float64(period) + float64(random_min)
	return res, full_res
}

func main() {
	var random_arr []float64
	var random_full []int64
	var x0 int64 = time.Now().UnixNano() / int64(time.Millisecond)
	normal_num, full_num := rnd(x0)
	random_full = append(random_full, full_num)
	random_arr = append(random_arr, normal_num)
	var i int64

	for i = 1; i < random_count; i++ {
		normal_num, full_num := rnd(random_full[i-1])
		random_full = append(random_full, full_num)
		random_arr = append(random_arr, normal_num)
	}
	fmt.Println("Псевдовипадкові числа: ")
	fmt.Printf("%v", random_arr)
}
