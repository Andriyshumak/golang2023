package main

import "fmt"

func main() {
	var a, b int
	fmt.Printf("a=")
	n, err := fmt.Scanf("%d\n", &a)
	if err != nil || n != 1 {
		fmt.Println(n, err)
	}
	fmt.Printf("b=")
	n, err = fmt.Scanf("%d\n", &b)
	if err != nil || n != 1 {
		fmt.Println(n, err)
	}
	fmt.Printf("a+b=%d", a+b)
}
