package main

func main() {
	userInt := -10                     // Цілочисельна знакова змінна
	userfloat64 := -3.4                // Дійсна змінна
	third := true                      //Булева змінна
	first := 0.37 + 1.72i              //Комплексна змінна
	first := "First: line 1\nline 2"   // Строкова змінна
	second := `Second: line 1\nline 2` // Строкова змінна
	letter := 'A'                      // Символьна змінна
	array := [5]int{1, 7, 4, 6, 3}     // Масив типу [5]int
}
