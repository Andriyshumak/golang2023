package functions

func Average(a float64, b float64, c float64) float64 {
	return (a + b + c) / 3
}

func Min(a float64, b float64, c float64) float64 {
	if a < b && a < c {
		return a
	}
	if b < a && b < c {
		return b
	}
	return c
}

func Equation(a float64, b float64) float64 {
	return b / a
}
