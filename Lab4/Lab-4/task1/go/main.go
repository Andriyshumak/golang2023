package main

import (
	"fmt"
	"strconv"

	"github.com/andlabs/ui"

	_ "github.com/andlabs/ui/winmanifest"
)

func count(material int, height int, width int, dop1 int, dop2 bool) float64 {
	var price1 float64 = 0
	if material == 0 && dop1 == 0 {
		price1 = 2.5
	} else if material == 0 && dop1 == 1 {
		price1 = 3
	} else if material == 1 && dop1 == 0 {
		price1 = 0.5
	} else if material == 1 && dop1 == 1 {
		price1 = 1
	} else if material == 2 && dop1 == 0 {
		price1 = 1.5
	} else if material == 2 && dop1 == 1 {
		price1 = 2
	}
	res := price1 * float64(height) * float64(width)
	if dop2 {
		res += 350
	}
	return res
}

func initGUI() {
	window := ui.NewWindow("Hello", 1000, 400, false)
	mainRow := ui.NewHorizontalBox()
	col1 := ui.NewVerticalBox()
	col1.Append(ui.NewLabel("Розмір вікна"), false)

	width := ui.NewEntry()
	height := ui.NewEntry()

	row := ui.NewHorizontalBox()
	row.Append(ui.NewLabel("Ширина, см"), false)
	row.Append(width, false)
	col1.Append(row, false)

	row = ui.NewHorizontalBox()
	row.Append(ui.NewLabel("Висота, см"), false)
	row.Append(height, false)
	col1.Append(row, false)

	row = ui.NewHorizontalBox()
	combobox1 := ui.NewCombobox()
	combobox1.Append("Дерево")
	combobox1.Append("Метал")
	combobox1.Append("Металопластик")
	combobox1.SetSelected(0)
	row.Append(ui.NewLabel("Матеріал"), false)
	row.Append(combobox1, false)
	col1.Append(row, false)
	mainRow.Append(col1, false)

	col2 := ui.NewVerticalBox()
	col2.Append(ui.NewLabel("Склопакет"), false)

	row = ui.NewHorizontalBox()
	combobox2 := ui.NewCombobox()
	combobox2.Append("Однокамерні")
	combobox2.Append("Двокамерні")
	combobox2.SetSelected(0)
	row.Append(combobox2, false)
	col2.Append(row, false)

	row = ui.NewHorizontalBox()
	checkbox := ui.NewCheckbox("Підвіконня")
	row.Append(checkbox, false)
	col2.Append(row, false)
	mainRow.Append(col2, false)

	row = ui.NewHorizontalBox()
	button := ui.NewButton("Розрахувати")
	price := ui.NewLabel("")
	row.Append(price, false)
	row.Append(button, false)
	button.OnClicked(func(*ui.Button) {
		hValue, err := strconv.Atoi(height.Text())
		wValue, err := strconv.Atoi(width.Text())

		res := count(combobox1.Selected(), hValue, wValue, combobox2.Selected(), checkbox.Checked())
		price.SetText("Price: " + fmt.Sprintf("%f", res) + " грн")

		if err != nil {
			// ... handle error
			panic(err)
		}
	})

	body := ui.NewVerticalBox()
	body.Append(mainRow, false)
	body.Append(row, false)

	window.SetChild(body)
	window.OnClosing(func(*ui.Window) bool {
		ui.Quit()
		return true
	})
	window.Show()
}
func main() {
	err := ui.Main(initGUI)
	if err != nil {
		panic(err)
	}
}
