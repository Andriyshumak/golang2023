package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Currency struct {
	name   string
	exRate float64
}

func newCurrency(name string, exRate float64) *Currency {
	c := Currency{name: name, exRate: exRate}
	return &c
}

func (c *Currency) SetName(name string) {
	c.name = name
}

func (c Currency) Name() string {
	return c.name
}

func (c *Currency) SetExRate(exRate float64) {
	c.exRate = exRate
}

func (c Currency) ExRate() float64 {
	return c.exRate
}

func (c Currency) Info() string {
	return strings.Join([]string{"Price:", strconv.Itoa(int(c.ExRate())), "Name:", c.Name()}, " ")
}

func (c Currency) ShowInfo() {
	fmt.Println(c.Info())
}

type Product struct {
	name     string
	price    float64
	cost     *Currency
	quantity int
	producer string
	weight   float64
}

func newProduct(name string, price float64, cost *Currency, quantity int, producer string, weight float64) *Product {
	p := Product{name: name, price: price, cost: cost, quantity: quantity, producer: producer, weight: weight}
	return &p
}

func (p *Product) SetName(name string) {
	p.name = name
}

func (p Product) Name() string {
	return p.name
}

func (p *Product) SetPrice(price float64) {
	p.price = price
}

func (p Product) Price() float64 {
	return p.price
}

func (p Product) Cost() *Currency {
	return p.cost
}

func (p *Product) SetCurrency(cost *Currency) {
	p.cost = cost
}

func (p Product) Quantity() int {
	return p.quantity
}

func (p *Product) SetQuantity(quantity int) {
	p.quantity = quantity
}

func (p Product) Producer() string {
	return p.producer
}

func (p *Product) SetProducer(producer string) {
	p.producer = producer
}

func (p Product) Weight() float64 {
	return p.weight
}

func (p *Product) SetWeight(weight float64) {
	p.weight = weight
}

func (p Product) GetPriceIn() float64 {
	return p.Price() * p.Cost().ExRate()
}

func (p Product) GetTotalPrice() float64 {
	return p.GetPriceIn() * float64(p.Quantity())
}

func (p Product) GetTotalWeight() float64 {
	return p.Weight() * float64(p.Quantity())
}

func ReadProduct() *Product {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Name: ")
	name, _ := reader.ReadString('\n')
	name = strings.Split(strings.TrimSpace(name), "\n")[0]

	fmt.Print("Price: ")
	price, _ := reader.ReadString('\n')
	price = strings.Replace(price, "\n", "", -1)
	f_price, _ := strconv.ParseFloat(strings.Split(strings.TrimSpace(price), "\n")[0], 64)

	fmt.Print("Quantity: ")
	quantity, _ := reader.ReadString('\n')
	quantity = strings.Replace(quantity, "\n", "", -1)
	i_quantity, _ := strconv.Atoi(strings.Split(strings.TrimSpace(quantity), "\n")[0])

	fmt.Print("Producer: ")
	producer, _ := reader.ReadString('\n')
	producer = strings.Split(strings.TrimSpace(producer), "\n")[0]

	fmt.Print("Weight: ")
	weight, _ := reader.ReadString('\n')
	weight = strings.Replace(weight, "\n", "", -1)
	f_weight, _ := strconv.ParseFloat(strings.Split(strings.TrimSpace(weight), "\n")[0], 64)

	fmt.Print("Cost name: ")
	cost_name, _ := reader.ReadString('\n')
	cost_name = strings.Split(strings.TrimSpace(cost_name), "\n")[0]

	fmt.Print("Cost exRate: ")
	ex_rate, _ := reader.ReadString('\n')
	ex_rate = strings.Replace(ex_rate, "\n", "", -1)
	f_ex_rate, _ := strconv.ParseFloat(strings.Split(strings.TrimSpace(ex_rate), "\n")[0], 64)

	cost := newCurrency(cost_name, f_ex_rate)
	return newProduct(name, f_price, cost, i_quantity, producer, f_weight)
}

func ReadProducts() []*Product {
	reader := bufio.NewReader(os.Stdin)

	var products []*Product

	for {
		product := ReadProduct()
		products = append(products, product)

		fmt.Print("Continue if 'yes': ")
		cont, _ := reader.ReadString('\n')
		cont = strings.Replace(cont, "\n", "", -1)

		if !strings.Contains(cont, "yes") {
			return products
		}

	}
}

func PrintProduct(p *Product) {
	info1 := strings.Join([]string{"Name:", p.Name(), "\nPrice", strconv.FormatFloat(p.Price(), 'f', -1, 64)}, " ")
	info2 := strings.Join([]string{"Cost_name:", p.Cost().Name(), "\nCost_price", strconv.FormatFloat(p.Cost().ExRate(), 'f', -1, 64)}, " ")
	info3 := strings.Join([]string{"Quantity:", strconv.Itoa(p.Quantity()), "\nProducer", p.Producer()}, " ")
	info4 := strings.Join([]string{"Weight:", strconv.FormatFloat(p.Weight(), 'f', -1, 64)}, " ")
	info5 := strings.Join([]string{"GetPriceIn:", strconv.FormatFloat(p.GetPriceIn(), 'f', -1, 64)}, " ")
	info6 := strings.Join([]string{"GetTotalPrice:", strconv.FormatFloat(p.GetTotalPrice(), 'f', -1, 64)}, " ")
	info7 := strings.Join([]string{"GetTotalPrice:", strconv.FormatFloat(p.GetTotalPrice(), 'f', -1, 64)}, " ")
	res := strings.Join([]string{info1, info2, info3, info4, info5, info6, info7}, "\n")
	fmt.Printf(res)
}

func PrintProducts(products []*Product) {
	for _, p := range products {
		PrintProduct(p)
		fmt.Printf("\n\n")
	}
}

func GetProductsInfo(products []*Product) (*Product, *Product) {
	p_min := products[0]
	p_max := products[0]
	for _, p := range products {
		if p_min.GetTotalPrice() > p.GetTotalPrice() {
			p_min = p
		}

		if p_max.GetTotalPrice() < p.GetTotalPrice() {
			p_max = p
		}
	}

	return p_min, p_max
}

func main() {
	products := ReadProducts()
	fmt.Printf("\n\n\nProducts\n")
	PrintProducts(products)
	fmt.Printf("\n\n\n")
	p1, p2 := GetProductsInfo(products)
	fmt.Printf("Min product\n")
	PrintProduct(p1)
	fmt.Printf("\n\n\n")
	fmt.Printf("Max product\n")
	PrintProduct(p2)
}
