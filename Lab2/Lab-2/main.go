package Lab2

import "fmt"
import funcs "Lab2/functions"

func main() {
	fmt.Println(funcs.Min(19, 25, -5.4))
	fmt.Println(funcs.Average(4, 7, 12))
	fmt.Println(funcs.Equation(-2, 4))
}
