package main

func main() {
	const limit = 512       // константа, сумісна з будь-якими числовими типами
	const top uint16 = 1421 // константа типу uint16
	const (
		Cyan    = 0
		Magenta = 1
		Yellow  = 2
	)
	const (
		Red   = iota // 0
		Green        // 1
		Blue         // 2
	)
}
