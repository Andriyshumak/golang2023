package main

func main() {
	var defaultInt32 int32                       // Цілочисельна знакова змінна
	var userInt uint32 = 10                      // Цілочисельна беззнакова змінна
	var defaultfloat32 float32                   // Дійсна змінна
	var third bool = true                        //Булева змінна
	var first complex64 = 0.37 + 1.72i           //Комплексна змінна
	var first string = "First: line 1\nline 2"   // Строкова змінна
	var second string = `Second: line 1\nline 2` // Строкова змінна
	var letter rune = 'A'                        // Символьна змінна
	var array [5]int = [5]int{1, 7, 4, 6, 3}     // Масив типу [5]int
}
