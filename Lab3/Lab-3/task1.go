package main

import (
	"fmt"
	"math"
	"time"
)

var random_min int64 = 0
var random_max int64 = 300
var random_count int64 = 10000
var a int64 = 69069
var c int64 = 1
var m int64 = (int64)(2 << 31)

func rnd(x int64) (int64, int64) {
	var full_res int64 = (a*x + c) % m
	var res int64 = full_res%(random_max-random_min) + random_min
	return res, full_res
}

var x int = 5

func do(x interface{}) {
	switch x.(type) {
	case float64:
		fmt.Printf("1 ")
	}
}
func main() {

	do(3.1415)
	var random_arr []int64
	var random_full []int64
	var x0 int64 = time.Now().UnixNano() / int64(time.Millisecond)
	normal_num, full_num := rnd(x0)
	random_full = append(random_full, full_num)
	random_arr = append(random_arr, normal_num)
	var i int64

	for i = 1; i < random_count; i++ {
		normal_num, full_num := rnd(random_full[i-1])
		random_full = append(random_full, full_num)
		random_arr = append(random_arr, normal_num)
	}

	var frequency map[int64]int64 = make(map[int64]int64)
	for i = 0; i < random_count; i++ {
		var val int64
		var ok bool
		val, ok = frequency[random_arr[i]]
		if !ok {
			val = 0
		}
		frequency[random_arr[i]] = val + 1
	}

	fmt.Println("\n\n\n")
	fmt.Println("Частота інтервалів: ")
	fmt.Printf("%v", frequency)

	var probability map[int64]float64 = make(map[int64]float64)
	for i = 0; i < (random_max - random_min); i++ {
		probability[i] = (float64)(float64(frequency[i]) / float64(random_count))
	}

	fmt.Println("\n\n\n")
	fmt.Println("Cтатистична ймовірність появи величини: ")
	fmt.Printf("%v", probability)

	var mathexpectation float64 = 0
	for i = 0; i < (random_max - random_min); i++ {
		mathexpectation += float64(i) * probability[i]
	}

	var dispersion float64 = 0
	for i = 0; i < (random_max - random_min); i++ {
		dispersion += math.Pow((float64(i)-mathexpectation), 2) * probability[i]
	}

	var sqrt float64 = math.Sqrt(dispersion)
	fmt.Println("\n\n\n")
	fmt.Println("Математичне сподівання: ", mathexpectation)
	fmt.Println("Дисперсія: ", dispersion)
	fmt.Println("Середньоквадратичне відхилення: ", sqrt)
}
